#include "frontend.h"

// Global variables! I didn't have any choice :(
WINDOW *topBorder, *leftBorder, *rightBorder, *bottomBorder;
BedeBestoon bedebestoon; // The main class!!!

void init()
{
    // Initialize window
    initscr();
    
    topBorder = newwin(3, 84, 0, 0); // The top one

    leftBorder = newwin(24, 42, 3, 0); // The left one

    rightBorder = newwin(24, 42, 3, 42); // The right one

    // Hotkeys hint!
    bottomBorder = newwin(3, 84, 27, 0);
    box(bottomBorder, 0, 0);
    mvwprintw(bottomBorder, 1, 8, "F:find | S:sort | C:compare | D:delete | l:login | A:add | E:edit");


    // Refresh to show borders
    refresh();
    wrefresh(bottomBorder);
}

void end()
{
    // Delete children windows
    delwin(topBorder);
    delwin(leftBorder);
    delwin(rightBorder);
    // Main window ends
    endwin();
    exit(0);
}

void topFunc(string username)
{
    // Top window function
    wclear(topBorder);    // Clear left window
    box(topBorder, 0, 0); // Draw a border for top window
    // Top window contains your username (if signed in)
    mvwprintw(topBorder, 1, 2, "BEDE BESTOON!");
    mvwprintw(topBorder, 1, 40, "Username: %s", username.c_str());
    // Ohhh! convert username to c_str because ncurses.h is a c library

    // Refresh top window to apply changes
    wrefresh(topBorder);
}

void leftFunc(vector <Advertise *> items, int selected)
{
    // Left window function
    wclear(leftBorder); // Clear left window
    box(leftBorder, 0, 0); // Draw a border for left window
    // Prints all items and hilight the selected one
    for (unsigned int i = 0; i < items.size(); ++i)
    {
        // Hilight selected
        if (i == selected)  wattron(leftBorder, A_REVERSE);

        // Print item
        mvwprintw(leftBorder, i + 1, 2, items[i]->getTitle());
        // Desable hilighting
        wattroff(leftBorder, A_REVERSE);
    }

    // Refresh and apply changes
    wrefresh(leftBorder);
}

void rightFunc(Advertise *ads, bool compare, pair <int, int> tag)
{
    wclear(rightBorder); // Clear right window
    box(rightBorder, 0, 0); // Draw a border for right window

    // Right window shows ads details
    ads->print(rightBorder);

    // Compare mode
    if (compare && ads->tag.first == tag.first) bedebestoon.compare(rightBorder, ads->tag, tag);

    wrefresh(rightBorder);
}

void loginMsg()
{
    // Login message
    char *input = new char; // Scanf can't get c++ string so i used char *
    WINDOW *message = newwin(8, 28, 10, 4);
    box(message, 0, 0);

    // Print message box!
    mvwprintw(message, 1, 2, "Enter username to login");
    mvwprintw(message, 3, 2, "usr: ");
    wattron(message, A_REVERSE);
    mvwprintw(message, 6, 11, " LOGIN ");
    wattroff(message, A_REVERSE);

    // Get input
    mvwgetnstr(message, 3, 7, input, 20);

    // Login (Or maybe sign up)
    bedebestoon.login(input);

    // Delete window and input pointer
    delete[] input;
    delwin(message);
}

void deleteMsg(pair <int, int> item)
{
    // Delete message
    char input;
    WINDOW *message = newwin(8, 37, 10, 3);
    box(message, 0, 0);

    // Print message box!
    mvwprintw(message, 1, 2, "Do you wanna delete?");
    mvwprintw(message, 2, 2, "It won't be deleted if not yours!");
    mvwprintw(message, 4, 2, "y/n: ");
    wattron(message, A_REVERSE);
    mvwprintw(message, 6, 15, " OK ");
    wattroff(message, A_REVERSE);

    // Get input
    mvwscanw(message, 4, 7, "%c", &input);

    // Delete (Or not)
    if (input == 'y' && bedebestoon.getUser() == bedebestoon.getAdvertise()[item.first]->getSender())
    {
        bedebestoon.deleteAds(bedebestoon.getAdvertise()[item.first]);
        bedebestoon.update();
    }

    // Delete window
    delwin(message);
    wrefresh(leftBorder);
}

void addMsg()
{
    // Add message
    int input;
    WINDOW *message = newwin(8, 37, 10, 3);
    box(message, 0, 0);

    // Print message box!
    mvwprintw(message, 1, 2, "What kind of ads?");
    mvwprintw(message, 2, 2, "1. Automobile");
    mvwprintw(message, 3, 2, "2. Estate");
    mvwprintw(message, 4, 2, "3. Electronic (laptop)");
    
    wattron(message, A_REVERSE);
    mvwprintw(message, 6, 15, " OK ");
    wattroff(message, A_REVERSE);

    // Get input
    mvwscanw(message, 1, 20, "%i", &input);

    // Delete window
    delwin(message);

    bedebestoon.addAds(rightBorder, input - 1); // Input in addAds sarts at ZERO :)
    bedebestoon.update(); // Update the list
}

void searchMsg()
{
    // Delete message
    char *input = new char;
    WINDOW *message = newwin(8, 37, 10, 3);
    box(message, 0, 0);

    // Print message box!
    mvwprintw(message, 1, 2, "Search: ");
    wattron(message, A_REVERSE);
    mvwprintw(message, 6, 15, " OK ");
    wattroff(message, A_REVERSE);

    // Get input
    mvwgetnstr(message, 1, 10, input, 25); string searchInput = input; // Store input!
    // Advanced search (by date)
    mvwprintw(message, 2, 2, "Advanced search(y/n)?");
    mvwscanw(message, 2, 25, "%c", input);

    if (*input == 'y' || *input == 'Y')
    {
        Date temp; // "temp" stores input date and after search will be destroyed
        mvwprintw(message, 3, 2, "Enter date(d/m/y):"); temp.edit(message, 3, 22);
        bedebestoon.advancedSearch(searchInput, temp);
    }
    else
    { bedebestoon.search(searchInput); }

    // Delete window
    delete[] input;
    delwin(message);
}

void inputReact(int ASCII, pair <int, int> &i, unsigned int pageSize, unsigned int pageNumber)
{
    // React to input
    // Change "selected" by arrow keys
    switch(ASCII)
    {
        // Check the input (first : element number AND second : page number)
        case KEY_UP:
        { i.first =  (i.first + pageSize - 1) % (pageSize); break; }

        case KEY_DOWN:
        { i.first =  (i.first + 1) % (pageSize); break; }

        case KEY_RIGHT:
        { i.second = (i.second + 1 ) % pageNumber; i.first = 0 ;break; }

        case KEY_LEFT:
        { i.second = (i.second + pageNumber - 1) % pageNumber; i.first = 0; break;}

        default:
        { break; }

    }
}


void controller()
{
    // Item pair stores elements and page number
    pair <int, int> item, tag; // tag stores the element tag that we wanna compare
    bool compare = false; // compare mode

    // Enable arrow keys for left window
    keypad(leftBorder, true);
    
    // Initialize varibales
    item = {0, 0};
    int input = 0;
    unsigned int pageNumber = 1;


    while(true)
    {

        inputReact(input, item, bedebestoon.getAdvertise().size(), pageNumber);
        pageNumber = bedebestoon.setPage(item.second);
        // left and right functions reacts to input changes
        leftFunc(bedebestoon.getAdvertise(), item.first);
        rightFunc(bedebestoon.getAdvertise()[item.first], compare, tag);
        topFunc(bedebestoon.getUser());

        switch(input)
        {
            case 'l':
            case 'L':
            {
                loginMsg(); // Show login message
                input = 0; // Flush input
                continue; // Ignore wgetch
            }

            case 'd':
            case 'D':
            {
                deleteMsg(item); // Show delete message
                input = KEY_UP; // Flush input and set it to up ( for some greedy reasons :)) )
                continue; // ignore wgetch
            }

            case 'e':
            case 'E':
            {
                bedebestoon.getAdvertise()[item.first]->edit(rightBorder);
                input = 0; // Flush input
                continue; // Ignore wgetch
            }

            case 'f':
            case 'F':
            {
                searchMsg(); // Show search message
                item.first = item.second = 0; // set item and page to 0,0
                input = 0; // Flush input
                continue; // Ignore wgetch
            }

            case 'a':
            case 'A':
            {
                addMsg();
                input = 0; // Flush input
                continue; // Ignore wgetch
            }

            case 's':
            case 'S':
            {
                bedebestoon.sortByDate();
                input = 0; // Flush input
                continue; // Ignore wgech
            }

            case 'c':
            case 'C':
            {
                compare = (compare == true ? false : true);
                tag = bedebestoon.getAdvertise()[item.first]->tag;
                input = 0; // Flush input
                continue; // Ignore wgetch
            }
        }

        input = wgetch(leftBorder);
    }
}