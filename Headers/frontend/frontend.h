#ifndef FRONTEND_H
#define FRONTEND_H

#include <ncurses.h>
#include <vector>
#include <string>
#include "../backend/bedebestoon.h"
#include "../backend/advertise.h"

using std::pair;
using std::string;
using std::vector;


/* 
 * Front-End functions
*/

void init(); // Initialize windows
void end(); // Delete windows

void topFunc(string); // Top window function
void leftFunc(vector <Advertise *>, int); // Left window function
void rightFunc(Advertise *, bool, pair <int, int>); // Right window function

void loginMsg();
void deleteMsg(pair <int, int>);
void addMsg();
void searchMsg();

void inputReact(int, pair <int, int> &, unsigned int, unsigned int);

void controller();

#endif