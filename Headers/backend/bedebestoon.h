/* main class */
#ifndef BEDEBESTOON_H
#define BEDEBESTOON_H

#include <fstream>
#include <algorithm>
#include <vector>
#include <string>
#include "advertise.h"
#include "date.h"

using std::fstream;
using std::sort;
using std::string;
using std::vector;

class BedeBestoon
{
    private:
      string username;
      Empty empty; // Handling a bug! sorry i had no idea :(

      vector <AutomobileAds> automobile;
      vector <EstateAds> estate;
      vector <ElectronicalAds> laptop;

      // Easy access to all vectors (polymorphism)
      vector <Advertise *> advertise;
      // A vector that stores each page elements (a subset of advertise vector)
      vector <Advertise *> subset;

      template <typename T>
      void parseToVec(vector<T> &, string); // Parse binary file data to vector
      
      template <typename T>
      void parseToFile(vector<T> &, string); // Parse vector to binary file

    public:
      BedeBestoon();
      
      unsigned int setPage(unsigned int); // Parse advertise vector to pages
      void save();
      void update();

      void login(string);
      string getUser() const;

      void sortByDate();    
      void search(string);
      void advancedSearch(string, Date);
      void deleteAds(Advertise *);
      void addAds(WINDOW *, int);
      void compare(WINDOW *, pair <int, int>, pair <int, int>);

      vector <Advertise *> getAdvertise(); // It is not const because it must do sth before return value
};

#endif