#include "advertise.h"

/* Setters */
void Advertise::setSender(char const *s)
{ strncpy(sender, s, 20); }

void Advertise::setTitle(char const *s)
{ strncpy(title, s, 25); }

void Advertise::setDescription(char const *s)
{ strncpy(description, s, 40); /* Split and get only first 40 chars*/ }

void Advertise::setPhoneNum(char const *s)
{ strncpy(phoneNumber, s, 12); /* Phone number is a 12 digit number */ }

void Advertise::setLocation(char const *s)
{ strncpy(location, s, 40); }

void Advertise::setPrice(float i)
{ price = i; }


/* Advertise getters */
char const *Advertise::getSender() const
{ return sender; }

char const *Advertise::getTitle() const
{ return title; }

char const *Advertise::getDescription() const
{ return description; }

char const *Advertise::getPhoneNum() const
{ return phoneNumber; }

char const *Advertise::getLocation() const
{ return location; }

float Advertise::getPrice() const
{ return price; }

/********************
* Automobile advertise
********************/

void AutomobileAds::setKind(char const *s)
{ strncpy(kind, s, 12); }

void AutomobileAds::compare(WINDOW *window, const AutomobileAds &right)
{
    char output;
    output = (getPrice() < right.getPrice() ? '+' : ' ');
    mvwprintw(window, 12, 1, "%c", output);
}

void AutomobileAds::print(WINDOW *window) const
{
    // Bold title
    wattron(window, A_BOLD);
    mvwprintw(window, 2, 2, getTitle());
    wattroff(window, A_BOLD);

    mvwprintw(window, 4, 2, getDescription());
    
    mvwprintw(window, 6, 2, "Kind: %s", kind);

    mvwprintw(window, 8, 2, "Product date: "); productDate.print(window);
    
    mvwprintw(window, 10, 2, "Location: %s", getLocation());

    mvwprintw(window, 12, 2, "Price: %f", getPrice());

    mvwprintw(window, 14, 2, "Phone: %s",  getPhoneNum());
    
    mvwprintw(window, 18, 2, "Sent by: %s",  getSender()); 

    mvwprintw(window, 20, 2, "Posted at: "); postDate.print(window);
}

void AutomobileAds::edit(WINDOW *window)
{
    char *temp = new char;
    float floatTemp;
    wclear(window);    // Clear right window
    box(window, 0, 0); // Draw a border for right window

    // Reprint
    mvwprintw(window, 6, 2, "Kind:");
    mvwprintw(window, 8, 2, "Product date:");
    mvwprintw(window, 10, 2, "Location:");
    mvwprintw(window, 12, 2, "Price:");
    mvwprintw(window, 14, 2, "Phone:");
    mvwprintw(window, 18, 2, "Sent by: %s",  getSender());         
    mvwprintw(window, 20, 2, "Posted at:");

    // Edit fields
    mvwgetnstr(window, 2, 2, temp, 25); setTitle(temp); 
    mvwgetnstr(window, 4, 2, temp, 40); setDescription(temp);
    mvwscanw(window, 6, 8, "%s", temp); setKind(temp);
    productDate.edit(window, 8, 16);
    mvwgetnstr(window, 10, 12, temp, 40); setLocation(temp);
    mvwscanw(window, 12, 9, "%f", &floatTemp); setPrice(floatTemp);
    mvwscanw(window, 14, 9, "%s", temp); setPhoneNum(temp);
    postDate.edit(window, 20, 13);

    delete[] temp;
}

/********************
* Estate advertise
********************/

void EstateAds::setKind(char const *s)
{ strncpy(kind, s, 12); }

void EstateAds::setPosition(char const *s)
{ strncpy(position, s, 12); }    

void EstateAds::setArea(float n)
{ area =  n; }

int EstateAds::getArea() const
{ return area; }

void EstateAds::compare(WINDOW *window, const EstateAds &right)
{
    char output;
    output = (getPrice() < right.getPrice() ? '+' : ' ');
    mvwprintw(window, 14, 1, "%c", output);

    output = (getArea() > right.getArea() ? '+' : ' ');
    mvwprintw(window, 8, 1, "%c", output);
}

void EstateAds::print(WINDOW *window) const
{
    // Bold title
    wattron(window, A_BOLD);
    mvwprintw(window, 2, 2, getTitle());
    wattroff(window, A_BOLD);

    mvwprintw(window, 4, 2, getDescription());
    
    mvwprintw(window, 6, 2, "Kind: %s", kind);

    mvwprintw(window, 8, 2, "Area: %i", area);

    mvwprintw(window, 10, 2, "Position: %s", position);   

    mvwprintw(window, 12, 2, "Location: %s", getLocation());

    mvwprintw(window, 14, 2, "Price: %f", getPrice());
    
    mvwprintw(window, 16, 2, "Phone: %s",  getPhoneNum());

    mvwprintw(window, 18, 2, "Sent by: %s",  getSender());        

    mvwprintw(window, 20, 2, "Posted at: "); postDate.print(window);
    // Refresh window to apply changes
    wrefresh(window);
}

void EstateAds::edit(WINDOW *window)
{
    char *temp = new char;
    float floatTemp;
    wclear(window);    // Clear right window
    box(window, 0, 0); // Draw a border for right window

    // Reprint
    mvwprintw(window, 6, 2, "Kind:");
    mvwprintw(window, 8, 2, "Area:");
    mvwprintw(window, 10, 2, "Position:");
    mvwprintw(window, 12, 2, "Location:");
    mvwprintw(window, 14, 2, "Price:");
    mvwprintw(window, 16, 2, "Phone:");
    mvwprintw(window, 18, 2, "Sent by: %s",  getSender());         
    mvwprintw(window, 20, 2, "Posted at:");

    // Edit fields
    mvwgetnstr(window, 2, 2, temp, 25); setTitle(temp); 
    mvwgetnstr(window, 4, 2, temp, 40); setDescription(temp);
    mvwscanw(window, 6, 8, "%s", temp); setKind(temp);
    mvwscanw(window, 8, 8, "%f", &floatTemp); setArea(floatTemp);
    mvwscanw(window, 10, 12, "%s", temp); setPosition(temp);
    mvwscanw(window, 12, 12, temp, 40); setLocation(temp);
    mvwscanw(window, 14, 9, "%f", &floatTemp); setPrice(floatTemp);
    mvwscanw(window, 16, 9, "%s", temp); setPhoneNum(temp);
    postDate.edit(window, 20, 13);

    delete[] temp;
}

/********************
* Electronic advertise
********************/

void ElectronicAds::setKind(char const *s)
{ strncpy(kind, s, 12); }

void ElectronicAds::setBrand(char const *s)
{ strncpy(brand, s, 12); }

char const *ElectronicAds::getKind() const
{ return kind; }

char const *ElectronicAds::getBrand() const
{ return brand; }

/********************
* Laptop advertise
********************/
void ElectronicalAds::setRam(int ram)
{ this->ram = ram; }

void ElectronicalAds::setCpu(int cpu)
{ this->cpu = cpu; }

int ElectronicalAds::getRam() const
{ return ram; }

int ElectronicalAds::getCpu() const
{ return cpu; }

void ElectronicalAds::compare(WINDOW *window, const ElectronicalAds &right)
{
    char output;
    output = (getRam() > right.getRam() ? '+' : ' ');
    mvwprintw(window, 10, 1, "%c", output);

    output = (getCpu() > right.getCpu() ? '+' : ' ');
    mvwprintw(window, 12, 1, "%c", output);
    
    output = (getPrice() < right.getPrice() ? '+' : ' ');
    mvwprintw(window, 16, 1, "%c", output);
}


void ElectronicalAds::print(WINDOW *window) const
{
    // Bold title
    wattron(window, A_BOLD);
    mvwprintw(window, 2, 2, getTitle());
    wattroff(window, A_BOLD);

    mvwprintw(window, 4, 2, getDescription());
    
    mvwprintw(window, 6, 2, "Kind: %s", getKind());

    mvwprintw(window, 8, 2, "Brand: %s", getBrand());

    mvwprintw(window, 10, 2, "Ram: %i", ram);

    mvwprintw(window, 12, 2, "Cpu: %i", cpu);

    mvwprintw(window, 14, 2, "Location: %s", getLocation());

    mvwprintw(window, 16, 2, "Price: %f", getPrice());
    
    mvwprintw(window, 18, 2, "Phone: %s",  getPhoneNum());

    mvwprintw(window, 20, 2, "Sent by: %s",  getSender());        

    mvwprintw(window, 22, 2, "Posted at: "); postDate.print(window);
    // Refresh window to apply changes
    wrefresh(window);
}

void ElectronicalAds::edit(WINDOW *window)
{
    char *temp = new char;
    float floatTemp;
    wclear(window);    // Clear right window
    box(window, 0, 0); // Draw a border for right window

    // Reprint
    mvwprintw(window, 6, 2, "Kind:");
    mvwprintw(window, 8, 2, "Brand:");
    mvwprintw(window, 10, 2, "Ram:");
    mvwprintw(window, 12, 2, "Cpu:");
    mvwprintw(window, 14, 2, "Location:");
    mvwprintw(window, 16, 2, "Price:");
    mvwprintw(window, 18, 2, "Phone:");
    mvwprintw(window, 20, 2, "Sent by: %s", getSender());         
    mvwprintw(window, 22, 2, "Posted at:");

    // Edit fields
    mvwgetnstr(window, 2, 2, temp, 25); setTitle(temp); 
    mvwgetnstr(window, 4, 2, temp, 40); setDescription(temp);
    mvwscanw(window, 6, 8, "%s", temp); setKind(temp);
    mvwscanw(window, 8, 9, "%s", temp); setBrand(temp);
    mvwscanw(window, 10, 7, "%f", &floatTemp); setRam(floatTemp); // Cast float to int
    mvwscanw(window, 12, 7, "%f", &floatTemp); setCpu(floatTemp); // Cast float to int
    mvwscanw(window, 14, 12, temp, 40); setLocation(temp);
    mvwscanw(window, 16, 9, "%f", &floatTemp); setPrice(floatTemp);
    mvwscanw(window, 18, 9, "%s", temp); setPhoneNum(temp);
    postDate.edit(window, 22, 13);

    delete[] temp;
}