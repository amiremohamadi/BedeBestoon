/*
 * Date class :) 
*/
#ifndef DATE_H
#define DATE_H

#include <iostream>
#include <ncurses.h>
#include <vector>

using std::vector;

class Date
{
    private:
      int year, month, day;

    public:
      Date(int = 1, int = 1, int = 2018);
      void setYear(int);
      void setMonth(int);
      void setDay(int);

      // Returns multi values contains year, month and day
      // It also keeps data away from direct access
      vector<int> get() const;

      void print(WINDOW *) const;
      void edit(WINDOW *, int, int);

      // Operators
      bool operator==(Date);
};

#endif