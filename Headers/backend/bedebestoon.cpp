#include "bedebestoon.h"

template <typename T>
void BedeBestoon::parseToVec(vector<T> &list, string s)
{
    // Define and open the file
    fstream db;
    db.open(s, std::ios::binary | std::ios::in);

    // If couldn't open the file
    if (!db.is_open())
    { std::cout << "Ohhh!\nSome problem happend with " << s << std::endl; exit(0); }

    // Reading data
    T temp;
    int counter = 0; // Used in seekg

    while (true)
    {
        // Go to the correct position
        db.seekg(sizeof(T) * counter, std::ios::beg);
        // Reading data from db to T (T can be a class)
        db.read((char *)&temp, sizeof(T));
        
        // Check end of file
        if (db.eof()) break;

        counter++;

        // psuh temp to the list
        list.push_back(temp);
    }
    // Close file
    db.close();
}

template <typename T>
void BedeBestoon::parseToFile(vector<T> &list, string s)
{
    // Define and open the file
    fstream db;
    db.open(s, std::ios::binary | std::ios::out);

    // If couldn't open the file
    if (!db.is_open())
    {
        std::cout << "Ohhh!\nSome problem happend with " << s << std::endl;
        exit(0);
    }

    // Writing data to db
    db.write((char *)(&list[0]), list.size() * sizeof(T));

    // Close file
    db.close();
}

/*******************
 * Public functions
********************/

BedeBestoon::BedeBestoon()
{
    // Parse binary data to vectors
    parseToVec<AutomobileAds>(automobile, "Headers/database/automobile.bin");
    parseToVec<EstateAds>(estate, "Headers/database/estate.bin");
    parseToVec<ElectronicalAds>(laptop, "Headers/database/electronical.bin");

    // Push vectors data to a general vector (easy access)
    update();
}

unsigned int BedeBestoon::setPage(unsigned int pageNum)
{
    unsigned int size = advertise.size() / 18;
    size += (advertise.size() % 18 == 0 ? 0 : 1);

    if ((pageNum * 18) + 18 <= advertise.size())
        subset = vector<Advertise *>(&advertise[pageNum * 18], &advertise[(pageNum * 18) + 18]);
    else
        subset = vector<Advertise *>(&advertise[pageNum * 18], &advertise[advertise.size()]);


    size = (size == 0 ? 1 : size); // To solve a bug
    // Return size to use in frontend.cpp -> inputReact function
    return size;
}

inline void BedeBestoon::save()
{
    // Save data
    parseToFile<AutomobileAds>(automobile, "Headers/database/automobile.bin");
    parseToFile<EstateAds>(estate, "Headers/database/estate.bin");
    parseToFile<ElectronicalAds>(laptop, "Headers/database/electronical.bin");
}

inline void BedeBestoon::update()
{
    advertise.clear();
    // Push vectors data to a general vector (easy access)
    for (unsigned int i = 0; i < automobile.size(); ++i)
        advertise.push_back(&automobile[i]);
    
    for (unsigned int i = 0; i < estate.size(); ++i)
        advertise.push_back(&estate[i]);

    for (unsigned int i = 0; i < laptop.size(); ++i)
        advertise.push_back(&laptop[i]);
}

void BedeBestoon::login(string s)
{    
    // There's no database for users!
    // You enter a username and access to that user posts! (sorry for my bad english!)
    username = s;
}


string BedeBestoon::getUser() const
{ return username; }


void BedeBestoon::sortByDate()
{
    enum _date {DAY, MONTH, YEAR};

    // Sort by date using algorithm lib sort function
    sort(advertise.begin(), advertise.end(), [](Advertise *a, Advertise *b) {
        long dateA = a->postDate.get()[YEAR] * 365 + a->postDate.get()[MONTH] * 30 + a->postDate.get()[DAY];
        long dateB = b->postDate.get()[YEAR] * 365 + b->postDate.get()[MONTH] * 30 + b->postDate.get()[DAY];

        return (dateA > dateB);
    });
}


void BedeBestoon::search(string s)
{
    advertise.clear();
    string temp; // Stores title (to use find string member function)
    // Find titles that contain the search input
    for (unsigned int i = 0; i < automobile.size(); ++i)
    {
        temp = automobile[i].getTitle();
        if (temp.find(s) != string::npos) advertise.push_back(&automobile[i]);
    }
    
    for (unsigned int i = 0; i < estate.size(); ++i)
    {
        temp = estate[i].getTitle();
        if (temp.find(s) != string::npos) advertise.push_back(&estate[i]);
    }

    for (unsigned int i = 0; i < laptop.size(); ++i)
    {
        temp = laptop[i].getTitle();
        if (temp.find(s) != string::npos) advertise.push_back(&laptop[i]);
    }
}


void BedeBestoon::advancedSearch(string s, Date d)
{
    advertise.clear();
    string temp; // Stores title (to use find string member function)
    // Find titles that contain the search input and have same date
    for (unsigned int i = 0; i < automobile.size(); ++i)
    {
        temp = automobile[i].getTitle();
        if (temp.find(s) != string::npos && automobile[i].postDate == d)
            advertise.push_back(&automobile[i]);
    }

    for (unsigned int i = 0; i < estate.size(); ++i)
    {
        temp = estate[i].getTitle();
        if (temp.find(s) != string::npos && estate[i].postDate == d)
            advertise.push_back(&estate[i]);
    }

    for (unsigned int i = 0; i < laptop.size(); ++i)
    {
        temp = laptop[i].getTitle();
        if (temp.find(s) != string::npos && laptop[i].postDate == d)
            advertise.push_back(&laptop[i]);
    }
}

void BedeBestoon::deleteAds(Advertise *ads)
{
    enum _adsType {AUTOMOBILE, ESTATE, LAPTOP};

    switch(ads->tag.first)
    {
        case AUTOMOBILE:
        {automobile.erase(automobile.begin() + ads->tag.second); break;}

        case ESTATE:
        {estate.erase(estate.begin() + ads->tag.second); break;}

        case LAPTOP:
        {laptop.erase(laptop.begin() + ads->tag.second); break;}

        default: break;
    }

    save();
}

void BedeBestoon::addAds(WINDOW *window, int kind)
{
    // Get the kind of ads that you wanna add
    enum _kind {AUTOMOBILE, ESTATE, LAPTOP};

    switch(kind)
    {
        case AUTOMOBILE:
        {
            AutomobileAds temp;
            // Initialize ads tag (kind and number) and set sender
            temp.tag.first = AUTOMOBILE; temp.tag.second = automobile.size();
            temp.setSender(username.c_str());
            
            automobile.push_back(temp);
            automobile[automobile.size() - 1].edit(window);
            break;
        }

        case ESTATE:
        {
            EstateAds temp;
            // Initialize ads tag (kind and number) and set sender
            temp.tag.first = ESTATE; temp.tag.second = estate.size();
            temp.setSender(username.c_str());

            estate.push_back(temp);
            estate[estate.size() - 1].edit(window);
            break;
        }

        case LAPTOP:
        {
            ElectronicalAds temp;
            // Initialize ads tag (kind and number) and set sender
            temp.tag.first = LAPTOP; temp.tag.second = laptop.size();
            temp.setSender(username.c_str());

            laptop.push_back(temp);
            laptop[laptop.size() - 1].edit(window);
            break;
        }
    }

    save();
}

void BedeBestoon::compare(WINDOW *window, pair <int, int> tag1, pair <int, int> tag2)
{
    enum _kind {AUTOMOBILE, ESTATE, LAPTOP };

    // First tag is kind
    switch(tag1.first)
    {
        case AUTOMOBILE:
        { automobile[tag1.second].compare(window, automobile[tag2.second]); break; }

        case ESTATE:
        { estate[tag1.second].compare(window, estate[tag2.second]); break; }
        
        case LAPTOP:
        { laptop[tag1.second].compare(window, laptop[tag2.second]); break; }

        default: break;
    }
}

vector <Advertise *> BedeBestoon::getAdvertise()
{
    // Check if list is empty (To solve a little bug!)
    if (subset.empty()) subset.push_back(&empty);
    return subset;
}