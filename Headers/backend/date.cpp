#include "date.h"

#define _YEAR 2009 // 2018 defined as floor value for year


Date::Date(int day, int month, int year)
{
    setDay(day);
    setMonth(month);
    setYear(year);
}

void Date::setYear(int year)
{
    if (year >=  _YEAR)
        this->year = year;
}

void Date::setMonth(int month)
{
    if (month >=  1 && month <= 12)
        this->month = month;
}

void Date::setDay(int day)
{
    if (day >=  1 && day <= 31)
        this->day = day;
}

// Returns multiple values
vector<int> Date::get() const
{ return {day, month, year}; }


void Date::print(WINDOW *window) const
{ wprintw(window, "%i/%i/%i", day, month, year);}

bool Date::operator==(Date right)
{
    enum _date {DAY,MONTH, YEAR};

    return ((get()[DAY] == right.get()[DAY]) && (get()[MONTH] == right.get()[MONTH]) 
            && (get()[YEAR] == right.get()[YEAR]));
}

void Date::edit(WINDOW *window, int y, int x)
{
    int day, month, year;
    mvwscanw(window, y, x, "%i", &day);
    mvwscanw(window, y, x + 3, "%i", &month);
    mvwscanw(window, y, x + 6, "%i", &year);

    setDay(day);
    setMonth(month);
    setYear(year);
}