#ifndef ADVERTISE_H
#define ADVERTISE_H

#include <iostream>
#include <ncurses.h>
#include <string.h>
#include "date.h"

using std::pair;
using std::string;

class Advertise
{
    private:
      char title[25], description[40], phoneNumber[12], location[40], sender[20];
      int price;

    public:
      Date postDate; // Don't worry! Date members are also private
      pair <int, int> tag; // Give every ads a tag! (it'll use when we wanna delete an ads)      

      // Functions
      void setSender(char const *);
      void setTitle(char const *);
      void setDescription(char const *);
      void setPhoneNum(char const *);
      void setLocation(char const *);
      void setPrice(float);

      pair <int, char[3]> getTag() const;
      char const *getSender() const;
      char const *getTitle() const;
      char const *getDescription() const;
      char const *getPhoneNum() const;
      char const *getLocation() const;
      float getPrice() const;

      virtual void print(WINDOW *) const = 0;
      virtual void edit(WINDOW *) = 0;
};

// Automobile advertise
class AutomobileAds : public Advertise
{
    private:
      char kind[12];

    public:
      Date productDate; // Product date (public!!! don't worry it's safe!)
      
      void setKind(char const *);
      void compare(WINDOW *, const AutomobileAds &);

      virtual void print(WINDOW *) const;
      virtual void edit(WINDOW *);
};

// Estate advertise
class EstateAds : public Advertise
{
    private:
      char kind[12], position[12];
      int area;

    public:
      void setKind(char const*);
      void setPosition(char const*);
      void setArea(float);
      int getArea() const;
      void compare(WINDOW *, const EstateAds &);

      virtual void print(WINDOW *) const;
      virtual void edit(WINDOW *);
};

// Electronic advertise
class ElectronicAds : public Advertise
{
    private:
      char kind[12], brand[12];

    public:
      void setKind(char const *);
      void setBrand(char const *);

      char const *getKind() const;
      char const *getBrand() const;
};

// Laptop advertise
class ElectronicalAds : public ElectronicAds
{
    private:
      int ram, cpu;

    public:     
      void setRam(int);
      void setCpu(int);
      int getRam() const;
      int getCpu() const;
      void compare(WINDOW *, const ElectronicalAds &);

      virtual void print(WINDOW *) const;
      virtual void edit(WINDOW *);
};

// Empty advertise (for handling a bug! sorry but i had no idea)
class Empty : public Advertise
{
    public:
      virtual void print(WINDOW *) const {};
      virtual void edit(WINDOW *){}
};

#endif