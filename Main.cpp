/*
* "Bede Bestoon" is a ncurses-based "fake" service for immediate trading :))
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Under GPL v3.0 License (General Public License)
* AMIR MOHAMMADI
*/

#include "Headers/frontend/frontend.h"

int main()
{
    // Resize terminal console
    std::cout << "\e[8;30;84t";
    
    init();
    controller();
    end();

    return 0;
}